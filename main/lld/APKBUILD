# Contributor: Eric Molitor <eric@molitor.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=lld
pkgver=15.0.1
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="The LLVM Linker"
url="https://llvm.org/"
# cannot link anything and fails
# ld.lld: error: unknown emulation: elf64_s390
# ld.lld: error: src/gn/gn_main.o: could not infer e_machine
# from bitcode target triple s390x-alpine-linux-musl
# also fails hundreds of tests
arch="all !s390x"
license="Apache-2.0"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	clang
	cmake
	libedit-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	llvm$_llvmver-test-utils
	llvm-libunwind-dev
	samurai
	zlib-dev
	"
checkdepends="gtest-dev bash llvm$_llvmver-test-utils"
subpackages="$pkgname-dbg $pkgname-libs $pkgname-dev $pkgname-doc"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/lld-$pkgver.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/cmake-$pkgver.src.tar.xz
	"
builddir="$srcdir/$pkgname-$pkgver.src"

case "$CARCH" in
armhf)
	# for some reason they hang forever, but the actual linker works fine
	options="$options !check"
	;;
esac

build() {
	CFLAGS="${CFLAGS/-g/-g1}" \
	CXXFLAGS="${CXXFLAGS/-g/-g1}" \
	CC=clang CXX=clang++ \
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_MODULE_PATH="$srcdir/cmake-$pkgver.src/Modules" \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DBUILD_SHARED_LIBS=ON \
		-DLLVM_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLLVM_EXTERNAL_LIT=/usr/bin/lit \
		-DLLD_BUILT_STANDALONE=ON
	cmake --build build
}

check() {
	ninja -C build check-lld
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 "$builddir"/docs/ld.lld.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
3f586c000f11364e3026536e77039366fb870f900d8cd76b64f6e8db40777af9af8533d3190eab145bcd9f4a8b8e43198b640b79809d034dbd730f82fc3f366d  lld-15.0.1.src.tar.xz
fbb29395a337be4e591567cc0f990857a2663cb2335b5ef30945c6b516dbc65e86f022ef3acc1dc572cf6791e1cd20f6754256e00b60cdbf579c04ed74460522  cmake-15.0.1.src.tar.xz
"
