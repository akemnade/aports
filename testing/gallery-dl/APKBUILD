# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=gallery-dl
pkgver=1.23.1
pkgrel=0
pkgdesc="CLI tool to download image galleries"
url="https://github.com/mikf/gallery-dl"
arch="noarch"
license="GPL-2.0-or-later"
depends="
	py3-requests
	python3
	"
makedepends="py3-setuptools"
checkdepends="py3-pytest yt-dlp"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/mikf/gallery-dl/archive/v$pkgver.tar.gz"

build() {
	python3 setup.py build

	make man completion
}

check() {
	make test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	# Install fish completion to the correct directory
	rm -r "$pkgdir"/usr/share/fish/vendor_completions.d
	install -Dm644 data/completion/gallery-dl.fish \
		-t "$pkgdir"/usr/share/fish/completions
}

sha512sums="
898fd952cc6e590a11e4154449153bd48547c9f21cba6864c95953f9be18ad27391d8033e5350d41714b28ded5711ddefefd4ca0f3c13016972a16b028a0fc1d  gallery-dl-1.23.1.tar.gz
"
