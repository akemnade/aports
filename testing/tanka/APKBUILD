# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=tanka
pkgver=0.23.0
pkgrel=0
pkgdesc="Flexible, reusable and concise configuration for Kubernetes"
url="https://tanka.dev"
# blocked by kubectl (kubernetes)
arch="x86_64 aarch64 armv7 x86"
license="Apache-2.0"
depends="kubectl"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/grafana/tanka/archive/refs/tags/v$pkgver.tar.gz"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v -o tk \
		-ldflags "-X github.com/grafana/tanka/pkg/tanka.CURRENT_VERSION=v$pkgver" \
		./cmd/tk
}

check() {
	# /pkg/helm test requires helm.
	# Tests timeout with helm installed.
	go test $(go list ./... | grep -v /pkg/helm)
}

package() {
	install -Dm755 tk -t "$pkgdir"/usr/bin/
}

sha512sums="
eb62fbe5b96e5cec8b8d13fc81a809213efdd83dffc06c2a054b332fcddfc1a7c4f94e1c4f91f08a000e7aedcb3d47753f150541ef56f6bf9b74d5c3dd69f864  tanka-0.23.0.tar.gz
"
